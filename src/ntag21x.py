PAGE_PWD = 43
PAGE_CFG0 = 41
PAGE_CFG1 = 42
PAGE_PACK = 44
PAGE_DATA = 39

def MFRC522_Auth_NTAG21x(self, key):
    buf = []
    buf.append(0x1B)

    for i in range(len(key)):
        buf.append(key[i])

    pOut = self.CalulateCRC(buf)
    buf.append(pOut[0])
    buf.append(pOut[1])

    (status, backData, backLen) = self.MFRC522_ToCard(self.PCD_TRANSCEIVE, buf)

    # Return the status and PACK
    return (status, backData[0:2])


def MFRC522_Write_NTAG21x(self, page, data):
    if len(data) != 4:
        raise ValueError("data must be a 4 byte array")
    buf = []
    buf.append(0xA2)
    buf.append(page)

    for i in range(4):
        buf.append(data[i])

    pOut = self.CalulateCRC(buf)
    buf.append(pOut[0])
    buf.append(pOut[1])

    (status, backData, backLen) = self.MFRC522_ToCard(self.PCD_TRANSCEIVE, buf)
    print(status, backData)

    return status