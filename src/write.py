from .config import get_all_secrets, CONFIG
from .secrets import calculate_pwd_auth, calculate_data
from .ntag21x import MFRC522_Auth_NTAG21x, MFRC522_Write_NTAG21x, PAGE_PACK, PAGE_CFG0, PAGE_CFG1, PAGE_DATA, PAGE_PWD
from time import sleep
import random

PROTECTED_STARTPAGE = PAGE_DATA

def write_card(reader):
    
    serial_bytes = reader.MFRC522_Read(0)[0:3] + reader.MFRC522_Read(0)[4:8]
    serial = ''.join(format(x, '02x') for x in serial_bytes)
    print("Card Serial: %s" % (serial))

    # try out passwords
    secrets = get_all_secrets()
    passwords = [
        [0xFF, 0xFF, 0xFF, 0xFF] # default
    ]
    for secret in secrets:
        (key, expected_pACK) = calculate_pwd_auth(bytearray(serial_bytes), secret)
        passwords.append(key)

    random.shuffle(passwords)

    is_authed = False
    for password in passwords:
        print("try password: ", password)
        (status, pACK) = MFRC522_Auth_NTAG21x(reader, password)
        if status == reader.MI_OK:
            print("success")
            is_authed = True
            break
        else:
            # reconnect
            # TODO: we have to reconnect after a failed attempt,
            # but this doesn't work somehow with the following code
            # we instead try next time again, but randomize the order of passwords
            # os that there is a chance to try out every password once
            break

            # (status,TagType) = reader.MFRC522_Request(reader.PICC_REQIDL)
            # if status != reader.MI_OK: 
            #     print("ERROR: could not reconnect after failed auth attempt (req)")
            # (status,uid) = reader.MFRC522_Anticoll()
            # if status != reader.MI_OK: 
            #     print("ERROR: could not reconnect after failed auth attempt (anticoll)")
            #     return
            # status = reader.MFRC522_SelectTag(uid)
            # print(status)
            
    
    if not is_authed:
        print("ERROR: could not authenticate tag '%s'" % serial)
        return
        
    (key, pACK) = calculate_pwd_auth(bytearray(serial_bytes), CONFIG['secret'])

    # write key
    status = MFRC522_Write_NTAG21x(reader, PAGE_PWD, key)
    if status != reader.MI_OK:
        print("ERROR: could not write key")
        return

    # write pACK
    status = MFRC522_Write_NTAG21x(reader, PAGE_PACK, [pACK[0], pACK[1], 0x00, 0x00])
    if status != reader.MI_OK:
        print("ERROR: could not write pACK")
        return

    # enable AUTH (CFG0)
    status = MFRC522_Write_NTAG21x(reader, PAGE_CFG0, [0b00000100, 0x00, 0x00, PROTECTED_STARTPAGE])
    if status != reader.MI_OK:
        print("ERROR: could not enable AUTH")
        return
        
    # enable R/W protection (CFG1)
    status = MFRC522_Write_NTAG21x(reader, PAGE_CFG1, [0b10000000, 0x00, 0x00, 0x00])
    if status != reader.MI_OK:
        print("ERROR: could not enable R/W protection")
        return
    
    # write data
    data = calculate_data(bytearray(serial_bytes), CONFIG['secret'])
    status = MFRC522_Write_NTAG21x(reader, PAGE_DATA, data)
    if status != reader.MI_OK:
        print("ERROR: could not write data")
        return

    print("card successfully initialized!\n")
    print("Note: you still need to add the serial number to")
    print("the list of allowed tags")

