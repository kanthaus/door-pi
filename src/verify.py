from .ntag21x import MFRC522_Auth_NTAG21x, PAGE_DATA
from .secrets import calculate_pwd_auth, calculate_data
from .metrics import log_error, log_success
from .config import is_accepted_serial, is_expired, CONFIG

def verify_card(reader, door):
    try:
        serial_bytes = reader.MFRC522_Read(0)[0:3] + reader.MFRC522_Read(0)[4:8]
        serial = ''.join(format(x, '02x') for x in serial_bytes)
        print("Card Serial: %s" % (serial))

        # Evidence 1: known UID?
        # checks, whether serial is on our whitelist
        if not is_accepted_serial(serial, door['id']):
            if is_expired(serial):
                log_error(door['id'], serial, 'expired')
            else:
                log_error(door['id'], serial, 'invalid_serial')
            return False

        # Evidence 2: authenticate
        # checks, whether tag is protected with the correct password    
        (key, expected_pACK) = calculate_pwd_auth(bytearray(serial_bytes), CONFIG['secret'])
        # key = [0xFF,0xFF,0xFF,0xFF]

        (status, pACK) = MFRC522_Auth_NTAG21x(reader, key)
        if status != reader.MI_OK:
            log_error(door['id'], serial, 'invalid_pwd')
            return False

        # Evidence 3: verify pACK
        # checks, whether tag knows the correct authentication response (pACK)
        if pACK[0] != expected_pACK[0] or pACK[1] != expected_pACK[1]:
            print("ERROR: pACK response is wrong got %s expected %s" % (pACK, expected_pACK));
            log_error(door['id'], serial, 'invalid_pack')
            return False

        # Evidence 4: verify stored data
        # there are some bytes stored on the tag, which get compared as an additional prove
        data = reader.MFRC522_Read(PAGE_DATA)[0:4]
        expected_data = calculate_data(bytearray(serial_bytes), CONFIG['secret'])
        if data != expected_data:
            print("ERROR: stored data is wrong. got %s expected %s" % (data, expected_data));
            log_error(door['id'], serial, 'invalid_data')
            return False

        print("success. welcome home!")
        log_success(door['id'], serial)
        return True

    except Exception as e:
        print("Error while verifying:")
        print(e)
        return False