import datetime
from .config import CONFIG



# TODO: write to influxdb

file = open(CONFIG['logfile'], "a")

def log_error(doorId, serial, error):
    time = datetime.datetime.now().isoformat()
    msg = "[%s][%s] Error while checking %s: %s" % (time, doorId, serial, error)
    print(msg)
    file.write(msg+"\n")
    pass

def log_success(doorId, serial):
    time = datetime.datetime.now().isoformat()
    msg = "[%s][%s] Welcome home %s" % (time, doorId, serial)
    print(msg)
    file.write(msg+"\n")
    pass