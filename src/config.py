from pathlib import Path
import os.path
import yaml
from datetime import date

tags_path = os.path.join(os.path.dirname(__file__), "../tags.yaml")

by_doors = {}

expired_tags = []

def update_tags():
    global by_doors, expired_tags

    tags_content = yaml.load(Path(tags_path).read_text(), Loader=yaml.FullLoader)
    by_doors = {}
    expired_tags = []
    present = date.today()
    for tag in tags_content['tags']:
        if tag['expiresAt'] < present:
            expired_tags.append(tag['serial'])
            continue
        allowedFor = []
        for door in tag['allowedFor']:
            if tags_content['groups'][door]:
                allowedFor += tags_content['groups'][door]
            else:
                allowedFor.append(door)
        
        for door in allowedFor:
            if not door in by_doors:
                by_doors[door] = list()
            
            by_doors[door].append(tag['serial'])

def is_accepted_serial(serial, door):
    update_tags()
    if not door in by_doors:
        print("WARN: there is not a single allowed tag for '%s'" % door)
        return False
    if serial in by_doors[door]:
        return True
    return False

def is_expired(serial):
    if serial in expired_tags:
        return True
    else:
        return False

def get_all_secrets():
    secrets = CONFIG['other_secrets'] if 'other_secrets' in CONFIG else []
    if not CONFIG['secret'] in secrets:
        secrets.append(CONFIG['secret'])
    return secrets

try:
    config_path = os.path.join(os.path.dirname(__file__), "../config.yaml")
    CONFIG = yaml.load(Path(config_path).read_text(), Loader=yaml.FullLoader)
    update_tags()

except yaml.YAMLError as error:
    print(error)
    sys.exit(1)

