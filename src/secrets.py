import hashlib

def calculate_pwd_auth(serial, secret):
    h = hashlib.blake2s(key=bytes(secret, 'utf-8'))
    h.update(serial)
    out = h.digest()
    pwd = list(out[0:4])
    pack = list(out[5:7])
    return (pwd, pack)

def calculate_data(serial, secret):
    h = hashlib.blake2s(key=bytes(secret, 'utf-8'))
    h.update(serial)
    out = h.digest()
    data = list(out[8:12])
    return data
