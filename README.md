# Kanthaus RFID Door

## Features
- RFID tag verification with all security features possible with NTAG213
- tag expiration

## Hardware
- Raspberry Pi
- MFRC522 Module
- [NTAG213](https://www.nxp.com/docs/en/data-sheet/NTAG213_215_216.pdf) tags
- small pcb with 
    * connectors 
    * tiny step down for 5V 
    * tiny step down's for door openers (`ENABLE` pin controlled by the pi)

## Setup on a pi

```sh
# enable SPI
sudo raspi-config

# install dependencies
sudo apt install -y git python3-pip

# download door-pi
git clone https://gitlab.com/kanthaus/door-pi.git ~/door-pi

# install python dependencies
cd ~/door-pi
pip install -r requirements.txt

# manually download tags.yaml file from kanthaus-private
curl --header "PRIVATE-TOKEN: [API ACCESS TOKEN]" "https://gitlab.com/api/v4/projects/3989213/repository/files/door-tags.yaml/raw" > ~/door-pi/tags.yaml

# create config
cp config.yaml.sample config.yaml

# manually
# - add a cronjob for upating the tags.yaml regulary
# - set secret in config (can be found in the keepass)

# check whether script starts without errors
python3 door.py

# install service
mv ~/door-pi/door.service /etc/systemd/system/door.service

# start & enable service
systemctl start door
systemctl enable door
```