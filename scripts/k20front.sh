#!/bin/bash
sudo systemctl kill -s SIGUSR1 door
# when the systemd service is not used, the following line helps instead:
#sudo killall -SIGUSR1 python3
echo UNLOCKED
sleep 2
