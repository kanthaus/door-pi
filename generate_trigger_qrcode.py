# It configures a SSH connection. For each QR code, a new public/private key pair is generated. The private key is only included in the QR code, the public
# key needs to be added to the authorized_keys file on the pi. Remember to set appropriate access rights, e.g.:
# no-port-forwarding,no-x11-forwarding,no-agent-forwarding,command="/home/pi/bin/controldoor.sh" ssh-[keyver] [pubkeydata] [comment]
import json
import qrcode
import time
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import ed25519
from datetime import timedelta
from datetime import date
import sys

if len(sys.argv) < 3:
    print('Specify the user name as first argument and door id as second argument!')
    sys.exit(1)
name = sys.argv[1]
door = sys.argv[2]
private_key = ed25519.Ed25519PrivateKey.generate()
public_key = private_key.public_key()
public_der = public_key.public_bytes(
    encoding=serialization.Encoding.DER,
    format=serialization.PublicFormat.SubjectPublicKeyInfo
)
private_der = private_key.private_bytes(
    encoding=serialization.Encoding.DER,
    format=serialization.PrivateFormat.PKCS8,
    encryption_algorithm=serialization.NoEncryption()
)
# the data structure follows the setting structure from trigger.
data = {
        'host': '192.168.100.227',
        'keypair': {
            'type': 'ED25519',
            'privateKey': private_der.hex(),
            'publicKey': public_der.hex(),
            'encrypted': False,
        },
        'locked_pattern': 'LOCKED',
        'name': F"{door}",
        'open_command': F"/home/sshopendoor/bin/{door}.sh",
        'port': 22,
        'require_wifi': True,
        'ssids': 'wuppdays,kanthaus',
        'timeout': 5000,
        'unlocked_pattern': 'UNLOCKED',
        'user': 'sshopendoor',
        'type': 'SshDoorSetup'
        }

# trigger expects the keypair to be a json string...
data['keypair'] = json.dumps(data['keypair'])
json_string = json.dumps(data)
#print(json_string)
qr = qrcode.make(json_string)
qr.save(F"qr-{name}.png")
sys.stderr.write(F"qr code saved to qr-{name}.png\n")
public_ssh = public_key.public_bytes(
        encoding=serialization.Encoding.OpenSSH,
        format=serialization.PublicFormat.OpenSSH
        )
print(f"  - name: {name}\n    pubkey: {public_ssh.decode('ascii')}\n    expiresAt: {(date.today() + timedelta(days=180)).isoformat()}\n    allowedFor: [{door}]")
