import RPi.GPIO as GPIO
from mfrc522 import MFRC522
import signal
import sys
from src.verify import verify_card
from src.config import CONFIG
from src.write import write_card
from time import sleep

continue_reading = True

# Capture SIGINT for cleanup when the script is aborted
def end_read(signal,frame):
    global continue_reading
    print("Ctrl+C captured, ending read.")
    continue_reading = False
    GPIO.cleanup()

signal.signal(signal.SIGINT, end_read)
signal.signal(signal.SIGTERM, end_read)

if len(CONFIG['doors']) > 1:
    print("ERROR: more than 1 door configured. this is currently not supported")
    # TODO: implement door concurrency
    sys.exit(1)
if len(CONFIG['doors']) < 1:
    print("ERROR: no door configured")
    sys.exit(1)


door = CONFIG['doors'][0]

reader1 = MFRC522(bus=door['spidev_bus'], device=door['spidev_device'], spd=1000000)

# open door via SIGUSR1
# e.g. `systemctl kill -s SIGUSR1 door`
def open_doors(signal, frame):
    print("SIGUSR1 captured. opening door")
    GPIO.output(door['opener_pin'], 1)
    sleep(3)
    GPIO.output(door['opener_pin'], 0)
signal.signal(signal.SIGUSR1, open_doors)

mode = 'verify'
if len(sys.argv) > 1 and sys.argv[1] == '--write':
    mode = 'write'
    print("WRITE MODE")

GPIO.setup(door['opener_pin'], GPIO.OUT)

print("Door activated! waiting for lovely people :)")
while continue_reading:
    # Scan for cards    
    (status,TagType) = reader1.MFRC522_Request(reader1.PICC_REQIDL)

    # If a card is found
    if status == reader1.MI_OK:
        print("Card detected")
    else:
        sleep(.5)
        continue

    
    # Get the UID of the card
    (status,uid) = reader1.MFRC522_Anticoll()

    # If we have the UID, continue
    if status != reader1.MI_OK:
        continue
    
    print("Card read UID: %s" % (uid))
    print("Tag Type: %s" % (hex(TagType)))

    if mode == 'write':
        write_card(reader1)
    else:
        valid = verify_card(reader1, door)
        if valid:
            GPIO.output(door['opener_pin'], 1)
            sleep(3)
            GPIO.output(door['opener_pin'], 0)

