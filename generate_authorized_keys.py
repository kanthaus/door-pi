from pathlib import Path
import os.path
import sys
import yaml
from datetime import date

tags_path = os.path.join(os.path.dirname(__file__), "./tags.yaml")

def write_authorized_keys(fp):
    output = []

    tags_content = yaml.load(Path(tags_path).read_text(), Loader=yaml.FullLoader)
    present = date.today()
    for entry in tags_content['ssh']:
        if entry['expiresAt'] >= present:
            output.append({'name': entry['name'], 'pubkey': entry['pubkey'], 'door': entry['allowedFor']})
    
    for entry in output:
        if len(entry['door']) > 1:
            print(F"Implementation error: Multiple doors per user are not allowed for granting SSH permissions. Please generate one key per door!")
            exit(1)
        for door in entry['door']:
            if tags_content['groups'].get(door):
                print(F"Implementation error: groups ({door}) are not allowed for granting SSH permissions. Please generate one key per door!")
                exit(1)
            else:
                fp.write(F"no-port-forwarding,no-x11-forwarding,no-agent-forwarding,command=\"{CONFIG['ssh_script_path']}{door}.sh\" {entry['pubkey']} {entry['name']}\n")

try:
    config_path = os.path.join(os.path.dirname(__file__), "./config.yaml")
    CONFIG = yaml.load(Path(config_path).read_text(), Loader=yaml.FullLoader)
    fp = sys.stdout
    if CONFIG.get('ssh_authorized_keys'):
        fp = open(CONFIG['ssh_authorized_keys'], 'w')

    write_authorized_keys(fp)

except yaml.YAMLError as error:
    print(error)
    sys.exit(1)

